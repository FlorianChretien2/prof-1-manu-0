var express = require('express');
var router = express.Router();

var connectorSingle = require('../controllers/connectorSingle.js');
var connectorUrl = require('../controllers/connectorUrl.js');

router.use('/content/:slug', connectorUrl, connectorSingle);

router.get('/content/:slug', function(req, res, next) {
	// console.log('from content.js:');
	res.render('content', {  
		title: 'Content',
		datas: res.datasAirtable.datasArticle,
		single: true 
	});
});

module.exports = router;