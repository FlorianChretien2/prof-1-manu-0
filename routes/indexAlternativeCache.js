/*

Algo du cache : 

- [ ] Middleware appelé à la route (avant le connectorList par exemple)
- [ ] Condition à l’intérieur de la route (SI cache existe & cache pas périmé) pr voir si execute OU PAS connectorList
    - [ ] Si execute pas : recupération directement des données du cache
    - [ ] Si execute : comme avant utilise le controlleur de bas
        - [ ] Mettre en cache les données récupérées au niveau du controlleur (à chaque fois ?)

contenu de connectorCache : 
- [ ] recupérer ac flat-cache le cache correspondant 
- [ ] transforme en archi json comme ce que renvoie l’autre connector
- [ ] Utiliser la fonctionnalité “save” qui supprime les “non visited keys” à chaque enregistrement

Maxence & Hub

*/





var express = require('express');
var router = express.Router();
var flatCache = require('flat-cache');

var cache = flatCache.load('index-cache');





/* GET home page. */
router.get('/', function(req, res, next) {

	if(!cache.getKey('listeIndex')){
		var connectorList = require('../controllers/connectorList.js');
		router.use(connectorList);

		cache.setKey('listeIndex', {
			title: 'Node CMS ECV',
			datas: res.datasAirtable.datasList,
			categories: ["Lorem","Ipsum","Dolor","Sit"]
		})
		cache.save();


		console.log("DataByCache => ");
		console.log(dataByCache.listeIndex);


		console.log("DataByRes => ");
		console.log(res.datasAirtable.datasList);
		
	}

	var dataByCache = cache.getKey('listeIndex');

	res.render('index', dataByCache.listeIndex);

});

// console.log(cache);
module.exports = router;
