var express = require('express');
var router = express.Router();

var connectorListCategoryId = require('../controllers/connectorListCategoryId.js');

router.use(connectorListCategoryId);

/* GET category page. */
router.get('/category', function(req, res, next) {
	console.log('from listCategoryType.js:');
	console.log(res.datasAirtable.datasCategory);
	res.render('components/categorylist', { 
		title: 'Express hot reload',
		categories: res.datasAirtable.datasCategory
	});
});

module.exports = router;