var base = require('./connectorConfig.js');

var datas = {};

var connectorListCategoryId = function(req, res, next){

base.getCategory.eachPage(function page(records, fetchNextPage) {
    // This function (`page`) will get called for each page of records.
    var i = 0;
    
    records.forEach(function(record) {
    	i++;
        var id = record.id;
        datas[i] = {
            'data': record.fields
        };
    });

    // To fetch the next page of records, call `fetchNextPage`.
    // If there are more records, `page` will get called again.
    // If there are no more records, `done` will get called.
    fetchNextPage();

}, function done(err, records) {
    if (err) { console.error(err); return; }

    res.datasAirtable = {'datasCategory' : datas};
    next();
});

};

module.exports = connectorListCategoryId;