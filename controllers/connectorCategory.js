var base = require('./connectorConfig.js');

var datas = {};

var CategoryController = function(req, res, next){

    base.getCategory.eachPage(function page(records, fetchNextPage) {
        var i = 0;
    
        records.forEach(function(record) {
            console.log('Retrieved', record.get('name'));
            i++;
            var id = record.id;
            datas[i] = {
                'id': record.id,
                'name': record.get('name')
            };
        });
    
        fetchNextPage();
     
    }, function done(err, records) {
        if (err) { console.error(err); return; }
        res.datas = datas; 
        next();
    });

};


module.exports = CategoryController;
