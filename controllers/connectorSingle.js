var base = require('./connectorConfig.js');

var connectorSingle = function(req, res, next){

	base.getSingle.find(req.id, function(err, record) {
		if (err) {
			console.error(err);
			next(err);
		}

		res.datasAirtable = {"datasArticle" : record};

		next();
	});

};

module.exports = connectorSingle;
