var base = require('./connectorConfig.js');

var datas = {};

var connectorUrl = function(req, res, next){

base.getUrl.eachPage(function page(records, fetchNextPage) {
    records.forEach(function(record) {
    	console.log(record);
        datas[record.fields.slug] = {
            'id': record.id
		};
    });

    fetchNextPage();
 
}, function done(err, records) {
    if (err) { console.error(err); return; }
    req.id = datas[req.params.slug].id;
    next();
});

};

module.exports = connectorUrl;